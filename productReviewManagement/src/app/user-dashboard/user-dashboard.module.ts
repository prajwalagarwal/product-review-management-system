import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppMaterialModule } from '../app-material/app-material.module';
import { ErrorDialogComponent } from '../shared/error-dialog/error-dialog.component';
import { ErrorDialogService } from '../shared/error-dialog/error-dialog.service';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { SearchProductComponent } from './search-product/search-product.component';
import { UserDashboardRoutingModule } from './user-dashboard-routing.module';
import { ViewProductComponent } from './view-product/view-product.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { AddProductComponent } from './add-product/add-product.component';
import { AddReviewComponent } from './add-review/add-review.component';



@NgModule({
  declarations: [
    HomeComponent,
    ProductsComponent,
    SearchProductComponent,
    ViewProductComponent,
    ReviewsComponent,
    AddProductComponent,
    AddReviewComponent,
  ],
  imports: [
    AppMaterialModule,
    CommonModule,
    UserDashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  entryComponents:[
    ErrorDialogComponent,
    ErrorDialogService,
    AddProductComponent,
    AddReviewComponent
  ]
})
export class UserDashboardModule { }
