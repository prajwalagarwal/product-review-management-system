import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication-service/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  userName!: string
  stats: any;
  keyWord!: string
  valueToSearch!: string
  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.userName = localStorage.getItem('userName') || '';
    this.authenticationService.getStats().subscribe((res: any) => {
      this.stats = res.data;
    })
  }
  logOut() {
    localStorage.clear();
    this.router.navigate(['login-home']);
  }
  search(keyWord: string) {
    this.router.navigate(['user/search-product'], { queryParams: { keyWord: keyWord } });
  }
  goToProducts(){
    this.router.navigate(['user/products'])
  }

}
