import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/models/product.mode';
import { ProductService } from 'src/app/services/product-service/product.service';
import { AddProductComponent } from '../add-product/add-product.component';
import { Constants } from 'src/app/shared/constants/constants';

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.scss']
})
export class SearchProductComponent implements OnInit {
  valueToSearch!: string;
  keyWord!: string;
  userName!: string;
  searchResult!: Product[];
  filteredResult!: Product[];
  filters = ['All', 'Brand', 'Product Code', 'Product Name'];
  selectedFilter = this.filters[0];
  constructor(private _location: Location,private dialog:MatDialog,private toastrService: ToastrService,private router: Router, private route: ActivatedRoute, private productService: ProductService) { }

  ngOnInit(): void {
    this.searchData();
  }
  searchData() {
    this.keyWord = this.route.snapshot.queryParams['keyWord'] || '';
    this.userName = localStorage.getItem('userName') || '';
    this.productService.getSearchResults(this.keyWord).subscribe((res: any) => {
      this.searchResult = res.data;
      this.filteredResult = res.data;
    })
  }
  filterData() {
    this.filteredResult = [];
    if (this.selectedFilter == this.filters[0]) {
      this.filteredResult = this.searchResult;
    }
    else if (this.selectedFilter == this.filters[1]) {
      this.searchResult.forEach((element: Product) => {
        if (element.brand.indexOf(this.keyWord) != -1) {
          this.filteredResult.push(element);
        }
      })
    }
    else if (this.selectedFilter == this.filters[2]) {
      this.searchResult.forEach((element: Product) => {
        if (String(element.productCode).indexOf(this.keyWord) != -1) {
          this.filteredResult.push(element);
        }
      })
    }
    else {
      this.searchResult.forEach((element: Product) => {
        if (element.productName.indexOf(this.keyWord) != -1) {
          this.filteredResult.push(element);
        }
      })
    }
  }
  logOut() {
    localStorage.clear();
    this.router.navigate(['login-home']);
  }
  backClicked() {
    this._location.back();
  }

  search(keyWord: string) {
    this.router.navigate(['user/search-product'], { queryParams: { keyWord: keyWord } });
    this.searchData();
  }
  addProduct(){
    const dialog=this.dialog.open(AddProductComponent,{
      disableClose:true
    })

    dialog.afterClosed().subscribe((res:boolean)=>{
      if(res){
        this.toastrService.success(Constants.PRODUCT_ADDED, "Success");
        this.searchData();
      }
    })

  }
}
