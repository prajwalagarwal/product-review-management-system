import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ReviewService } from 'src/app/services/review-service/review.service';
import { ConfirmDeleteComponent } from 'src/app/shared/confirm-delete/confirm-delete.component';
import { AddProductComponent } from '../add-product/add-product.component';

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.scss']
})
export class AddReviewComponent implements OnInit {

  rating!:number
  reviewForm!: FormGroup;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,private router: Router, private matDialogRef: MatDialogRef<AddProductComponent>, private toastrService: ToastrService, private dialog: MatDialog, private fb: FormBuilder, private reviewService: ReviewService) { }

  ngOnInit(): void {
    this.initialiseForm();
  }
  initialiseForm() {
    this.reviewForm = this.fb.group({
      "reviewId": [''],
      "rating": ['', Validators.required],
      "heading": ['', Validators.required],
      "reviewComment": ['',[Validators.required,Validators.maxLength(400),Validators.minLength(20)]],
      "reviewStatus":[false]
    })
    console.log(this.reviewForm)
  }
  getErrorMessage(error:any){
    console.log(error)
    if(error.minlength){
      return "You have entered only "+error.minlength.actualLength+" characters. Min Characrters allowed is 20."
    }
    else if(error.maxlength){
      return "You have entered +"+error.maxlength.actualLength+" characters. Max Characrters allowed is 400." 
    }
    return ;

  }
  rateProduct() {
    const dialog = this.dialog.open(ConfirmDeleteComponent, {
      data: {
        text: "rate this Product?"
      }
    })
    dialog.afterClosed().subscribe((res: any) => {
      if (res) {
        this.reviewService.postReview(this.data.productCode,this.reviewForm.value).subscribe((res: any) => {
          this.matDialogRef.close(true);
        })
      }
      else {
        this.matDialogRef.close(false);
      }
    })
  }

}
