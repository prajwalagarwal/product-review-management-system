import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/models/product.mode';
import { Review } from 'src/app/models/Review.model';
import { ProductService } from 'src/app/services/product-service/product.service';
import { ReviewService } from 'src/app/services/review-service/review.service';
import { Constants } from 'src/app/shared/constants/constants';
import { AddReviewComponent } from '../add-review/add-review.component';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {
  userName!: string;
  productCode!: number;
  productDetails!: Product;
  approvedReviewList!: Review[];
  constructor(private _location: Location, private dialog: MatDialog, private toastrService: ToastrService, private router: Router, private route: ActivatedRoute, private reviewService: ReviewService, private productService: ProductService) { }

  ngOnInit(): void {
    this.getAllReview();
  }
  getAllReview() {
    this.productCode = this.route.snapshot.queryParams['product_code'] || '';
    this.userName = localStorage.getItem('userName') || '';
    this.approvedReviewList = [];
    this.productService.getProductsDetailsByProductCode(this.productCode).subscribe((res: any) => {
      if (res.message = "success") {
        this.productDetails = res.data;
        this.productDetails.review.forEach((review: Review) => {
          if (review.reviewStatus) {
            this.approvedReviewList.push(review);
          }
        })
      }
    })
  }
  logOut() {
    localStorage.clear();
    this.router.navigate(['login-home']);
  }
  addReview() {
    const dialog = this.dialog.open(AddReviewComponent, {
      disableClose: true,
      data:{
        "productCode":this.productCode
      }
    })

    dialog.afterClosed().subscribe((res: boolean) => {
      if (res) {
        this.toastrService.success(Constants.REVIEW_ADDED, "Success");
        this.getAllReview();
      }
    })
  }
  addProduct() {
    this.router.navigate(['user/products']);
  }
  backClicked() {
    this._location.back();
  }

}
