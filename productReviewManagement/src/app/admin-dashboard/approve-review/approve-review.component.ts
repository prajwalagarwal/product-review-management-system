import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Review } from 'src/app/models/Review.model';
import { ReviewService } from 'src/app/services/review-service/review.service';
import { ConfirmDeleteComponent } from 'src/app/shared/confirm-delete/confirm-delete.component';
import { Constants } from 'src/app/shared/constants/constants';

@Component({
  selector: 'app-approve-review',
  templateUrl: './approve-review.component.html',
  styleUrls: ['./approve-review.component.scss']
})
export class ApproveReviewComponent implements OnInit {
  dataSource!: Review[];
  displayedColumns: string[] = ['sno', 'heading', 'description', 'rating','action'];
  classes: any;


  constructor(private toastrService: ToastrService, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, private reviewService: ReviewService) { }

  ngOnInit() {
    this.getPendingReview();
  }

  private getPendingReview() {
    this.reviewService.getPendingReviews().subscribe((res: any) => {
      this.dataSource = res.data;
    });
  }

  approveReview(review: Review) {
    const dialog = this.dialog.open(ConfirmDeleteComponent, {
      data: {
        text: Constants.APPROVE_REVIEW
      }
    })
    dialog.afterClosed().subscribe((res: any) => {
      if (res) {
        this.reviewService.approveReview(review.reviewId, review).subscribe(res => {
          this.toastrService.success(Constants.REVIEW_APPROVED, "Success");
          this.getPendingReview();
        })
      }
    })
  }
  unApproveReview(review: Review) {
    const dialog = this.dialog.open(ConfirmDeleteComponent, {
      data: {
        text: Constants.DELETE_REVIEW
      }
    })
    dialog.afterClosed().subscribe((res: any) => {
      if (res) {
        this.reviewService.deleteReview(review.reviewId).subscribe(res => {
          this.toastrService.success(Constants.REVIEW_DELETED, "Success");
          this.getPendingReview();
        })
      }
    })
  }


}
