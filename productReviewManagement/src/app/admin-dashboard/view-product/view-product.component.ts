import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product.mode';
import { Review } from 'src/app/models/Review.model';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss']
})
export class ViewProductComponent implements OnInit {
  @Input() productList!: Product[]
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  getAverageRating(review: any) {
    let count=0;
    if (review && review.length > 0) {
      var averageReview: number = 0;
      review.forEach((el: any) => {
        if(el.reviewStatus){
          averageReview += el.rating;
          count++;
        }
      })
      return averageReview / count;
    }
    else {
      return 0;
    }
  }
  getNoOfReviews(review:any){
    let count=0;
    review.forEach((el: any) => {
      if(el.reviewStatus){
        count++;
      }
    })
    return count;
  }

  goToReview(productCode:number){
    this.router.navigate(['admin/reviews'], { queryParams: { product_code:productCode } });
  }

  addProduct(){
    this.router.navigate(['admin/products']);
  }

}
