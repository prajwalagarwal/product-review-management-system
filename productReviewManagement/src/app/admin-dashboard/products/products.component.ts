import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/models/product.mode';
import { ProductService } from 'src/app/services/product-service/product.service';
import { Constants } from 'src/app/shared/constants/constants';
import { AddProductComponent } from '../add-product/add-product.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  userName!:string;
  filteredResult!: Product[];
  valueToSearch!:string;

  constructor(private toastrService: ToastrService,private dialog:MatDialog,private router : Router,private route:ActivatedRoute,private productService:ProductService) { }

  ngOnInit(): void {
    this.userName = localStorage.getItem('userName') || '';
    this.getAllProductData();
  }
  getAllProductData(){
    this.productService.getAllProductsDetails().subscribe((res:any)=>{
      this.filteredResult=res.data;
    })
  }
  logOut() {
    localStorage.clear();
    this.router.navigate(['login-home']);
  }

  addProduct(){
    const dialog=this.dialog.open(AddProductComponent,{
      disableClose:true
    })

    dialog.afterClosed().subscribe((res:boolean)=>{
      if(res){
        this.toastrService.success(Constants.PRODUCT_ADDED, "Success");
        this.getAllProductData();
      }
    })

  }
  backClicked() {
   this.router.navigate(['admin']);
  }
  search(keyWord: string) {
    this.router.navigate(['admin/search-product'], { queryParams: { keyWord: keyWord } });
  }

}
