import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminDashboardRoutingModule } from './admin-dashboard-routing.module';
import { AddProductComponent } from './add-product/add-product.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { SearchProductComponent } from './search-product/search-product.component';
import { ViewProductComponent } from './view-product/view-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppMaterialModule } from '../app-material/app-material.module';
import { ErrorDialogComponent } from '../shared/error-dialog/error-dialog.component';
import { ErrorDialogService } from '../shared/error-dialog/error-dialog.service';
import { ApproveReviewComponent } from './approve-review/approve-review.component';



@NgModule({
  declarations: [
    HomeComponent,
    ProductsComponent,
    SearchProductComponent,
    ViewProductComponent,
    ReviewsComponent,
    AddProductComponent,
    ApproveReviewComponent,
  ],
  imports: [
    AppMaterialModule,
    CommonModule,
    AdminDashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  entryComponents:[
    ErrorDialogComponent,
    ErrorDialogService,
    AddProductComponent,
    ApproveReviewComponent
  ]
})
export class AdminDashboardModule { }
