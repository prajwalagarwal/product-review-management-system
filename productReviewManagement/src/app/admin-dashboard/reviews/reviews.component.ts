import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/models/product.mode';
import { Review } from 'src/app/models/Review.model';
import { ProductService } from 'src/app/services/product-service/product.service';
import { ReviewService } from 'src/app/services/review-service/review.service';
import { ConfirmDeleteComponent } from 'src/app/shared/confirm-delete/confirm-delete.component';
import { Constants } from 'src/app/shared/constants/constants';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {
  userName!: string;
  productCode!: number;
  productDetails!: Product;
  approvedReviewList!: Review[];
  unApprovedReviewList!: Review[];

  constructor(private _location: Location, private dialog: MatDialog, private toastrService: ToastrService, private router: Router, private route: ActivatedRoute, private reviewService: ReviewService, private productService: ProductService) { }

  ngOnInit(): void {
    this.getAllReview();
  }
  getAllReview() {
    this.productCode = this.route.snapshot.queryParams['product_code'] || '';
    this.userName = localStorage.getItem('userName') || '';
    this.approvedReviewList = [];
    this.unApprovedReviewList = [];

    this.productService.getProductsDetailsByProductCode(this.productCode).subscribe((res: any) => {
      if (res.message = "success") {
        this.productDetails = res.data;
        this.productDetails.review.forEach((review: Review) => {
          if (review.reviewStatus) {
            this.approvedReviewList.push(review);
          }
          else {
            this.unApprovedReviewList.push(review);
          }
        })
      }
    })
  }
  logOut() {
    localStorage.clear();
    this.router.navigate(['login-home']);
  }

  addProduct() {
    this.router.navigate(['admin/products']);
  }
  backClicked() {
    this._location.back();
  }
  approveReview(review:Review) {
    const dialog = this.dialog.open(ConfirmDeleteComponent, {
      data: {
        text: Constants.APPROVE_REVIEW
      }
    })
    dialog.afterClosed().subscribe((res: any) => {
      if (res) {
        this.reviewService.approveReview(review.reviewId,review).subscribe(res=>{
          this.toastrService.success(Constants.REVIEW_APPROVED, "Success");
          this.getAllReview();
        })
      }
    })
  }
  unApproveReview(review:Review) {
    const dialog = this.dialog.open(ConfirmDeleteComponent, {
      data: {
        text: Constants.DELETE_REVIEW
      }
    })
    dialog.afterClosed().subscribe((res: any) => {
      if (res) {
        this.reviewService.deleteReview(review.reviewId).subscribe(res=>{
          this.toastrService.success(Constants.REVIEW_DELETED, "Success");
          this.getAllReview();
        })
      }
    })
  }
}
