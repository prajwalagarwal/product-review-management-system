import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { SearchProductComponent } from './search-product/search-product.component';



const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'products', component: ProductsComponent },
  {
    path: 'search-product', component: SearchProductComponent,children:[
      {
        path:':keyWord',component:SearchProductComponent
      }
    ]
  },
  {
    path: 'reviews', component: ReviewsComponent,children:[
      {
        path:':product_code',component:ReviewsComponent
      }
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminDashboardRoutingModule { }
