import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication-service/authentication.service';
import { ReviewService } from 'src/app/services/review-service/review.service';
import { ApproveReviewComponent } from '../approve-review/approve-review.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  noOfreviews!:number
  userName!: string
  stats: any;
  keyWord!: string
  valueToSearch!: string
  constructor(private reviewService:ReviewService,private dialog: MatDialog, private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.userName = localStorage.getItem('userName') || '';
    this.authenticationService.getStats().subscribe((res: any) => {
      this.stats = res.data;
    })
    this.reviewService.getPendingReviews().subscribe((res: any) => {
      this.noOfreviews = res.data.length;
    });
  }
  logOut() {
    localStorage.clear();
    this.router.navigate(['login-home']);
  }
  search(keyWord: string) {
    this.router.navigate(['admin/search-product'], { queryParams: { keyWord: keyWord } });
  }
  goToProducts() {
    this.router.navigate(['admin/products'])
  }
  viewPendingReviews() {
    const dialog = this.dialog.open(ApproveReviewComponent, {
      disableClose: true,
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      panelClass: 'app-full-bleed-dialog'


    })
  }

}
