import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProductService } from 'src/app/services/product-service/product.service';
import { ConfirmDeleteComponent } from 'src/app/shared/confirm-delete/confirm-delete.component';
import { Constants } from 'src/app/shared/constants/constants';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  productForm!: FormGroup;
  constructor(private router: Router, private matDialogRef: MatDialogRef<AddProductComponent>, private toastrService: ToastrService, private dialog: MatDialog, private fb: FormBuilder, private productService: ProductService) { }

  ngOnInit(): void {
    this.initialiseForm();
  }
  initialiseForm() {
    this.productForm = this.fb.group({
      "productCode": ['', Validators.required],
      "productName": ['', Validators.required],
      "brand": ['', Validators.required],
      "review": [[]]
    })
  }
  submitProduct() {
    const dialog = this.dialog.open(ConfirmDeleteComponent, {
      data: {
        text: Constants.ADD_PRODUCT
      }
    })
    dialog.afterClosed().subscribe((res: any) => {
      if (res) {
        this.productService.addProductForReview(this.productForm.value).subscribe((res: any) => {
          this.matDialogRef.close(true);
        })
      }
      else {
        this.matDialogRef.close(false);
      }
    })
  }





  // submitProduct() {
  //   const dialog = this.dialog.open(ConfirmDeleteComponent, {
  //     data:{
  //       text:"add this Product?"
  //     }
  //   })
  //   dialog.afterClosed().subscribe((res: any) => {
  //     if (res) {
  //       this.productService.getProductsDetailsByProductCode(this.productForm.value.productCode).subscribe((res: any) => {
  //         if (res.message != 'success') {
  //           this.productService.addProductForReview(this.productForm.value).subscribe((res: any) => {
  //             this.matDialogRef.close(true);
  //           })
  //         }
  //         else {
  //           this.toastrService.error("Product already present with this code. Redirecting..Please Wait", "Success");
  //           this.router.navigate(['user/reviews'], { queryParams: { product_code: this.productForm.value.productCode } });
  //           this.matDialogRef.close(false);
  //           }
  //       })
  //     }
  //     else {
  //       this.matDialogRef.close(false);
  //     }
  //   })
  // }
}
