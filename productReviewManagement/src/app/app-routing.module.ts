import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './shared/guard/admin.guard';
import { RouteGuard } from './shared/guard/route.guard';
import { UserGuard } from './shared/guard/user.guard';

const routes: Routes = [
  {
    path: 'login-home', loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'user', loadChildren: () => import('./user-dashboard/user-dashboard.module').then(m => m.UserDashboardModule),
    canActivate: [RouteGuard, UserGuard]
  },
  {
    path: 'admin', loadChildren: () => import('./admin-dashboard/admin-dashboard.module').then(m => m.AdminDashboardModule),
    canActivate: [RouteGuard, AdminGuard]
  },
  {
    path: '', redirectTo: 'login-home', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
