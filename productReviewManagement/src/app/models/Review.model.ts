export class Review{
    reviewId!:number;
    rating!:number;
    heading!:string;
    reviewComment!:String;
    reviewStatus!:boolean;
}