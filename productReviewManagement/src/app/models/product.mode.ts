import { Review } from "./Review.model";

export class Product{
    productCode!:number
    productName!:String;
    brand!:String;
    review!:Set<Review>
}