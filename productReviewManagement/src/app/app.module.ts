import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppMaterialModule } from './app-material/app-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RequestInterceptorInterceptor } from './services/interceptor/request-interceptor.interceptor';
import { ConfirmDeleteComponent } from './shared/confirm-delete/confirm-delete.component';
import { ErrorDialogComponent } from './shared/error-dialog/error-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    ErrorDialogComponent,
    ConfirmDeleteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgbModule
  ],
  providers: [
    AppMaterialModule,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:RequestInterceptorInterceptor,
      multi:true
    }
  ],
  entryComponents:[
    ErrorDialogComponent,
    ConfirmDeleteComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
