export class Constants{
    public static BASE_URL="http://localhost:8099/";
    public static LOGIN_API_END_POINT="product.review.management/v1/user/";
    public static PRODUCT_API_END_POINT="product.review.management/v1/products/";
    public static REVIEW_API_END_POINT="product.review.management/v1/review/";


    public static RESET_PASSWORD="reset password";
    public static INVALID_CREDENTIALS="Invalid credentials";
    public static USER_REGISTERED="User registered successfully";
    public static ADD_PRODUCT="add this Product";
    public static PRODUCT_ADDED="Product added successfully";
    public static REVIEW_DELETED="Review deleted successfully";
    public static REVIEW_APPROVED="Review approved successfully";
    public static REVIEW_ADDED="Review added successfully. It will be displayed once approved.";
    public static APPROVE_REVIEW="approve this Review";
    public static DELETE_REVIEW="delete this Review";
    



}