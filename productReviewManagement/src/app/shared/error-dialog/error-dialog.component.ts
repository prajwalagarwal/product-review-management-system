import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})
export class ErrorDialogComponent implements OnInit {

  title = 'Angular-Interceptor';
  description!: string;
  constructor( private router:Router,@Inject(MAT_DIALOG_DATA) public data: any, private dialogRef: MatDialogRef<ErrorDialogComponent>,private dialog:MatDialog) {
      this.data = data;
  }

  ngOnInit(){
    if(this.data.productCode!=null){
      setTimeout(() => {
        this.dialog.closeAll();
        if(localStorage.getItem('userType')=='U')
          this.router.navigate(['user/reviews'], { queryParams: { product_code:this.data.productCode } });
        else
         this.router.navigate(['admin/reviews'], { queryParams: { product_code:this.data.productCode } });
    }, 5000);
    }

  }
  close() {
      this.dialogRef.close();
  }


}


