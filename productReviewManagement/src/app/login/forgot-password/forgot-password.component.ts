import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService } from 'src/app/services/authentication-service/authentication.service';
import { ConfirmDeleteComponent } from 'src/app/shared/confirm-delete/confirm-delete.component';
import { Constants } from 'src/app/shared/constants/constants';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  loginForm!: FormGroup;
  success!: boolean


  constructor(private dialog:MatDialog,private fb: FormBuilder, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.initialiseForm();
  }

  initialiseForm() {
    this.loginForm = this.fb.group({
      "email": ['', [Validators.required, Validators.email]],
      "password": ['', Validators.required]
    })
  }
  resetPassword() {
    const dialog = this.dialog.open(ConfirmDeleteComponent, {
      data: {
        text: Constants.RESET_PASSWORD,
      },
    });

    dialog.afterClosed().subscribe((value) => {
      if (value) {
        this.authenticationService.forgotPassword(this.loginForm.value).subscribe(res => {
          this.success = true;
        })
      }
    })
  }
}
