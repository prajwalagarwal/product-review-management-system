import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AuthenticationService } from 'src/app/services/authentication-service/authentication.service';
import { Constants } from 'src/app/shared/constants/constants';
import { ErrorDialogService } from 'src/app/shared/error-dialog/error-dialog.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(private errorService: ErrorDialogService, private dialog: MatDialog, private auth: AuthService, private router: Router, private fb: FormBuilder, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    if (this.auth.isLoggedIn() && localStorage.getItem('userType') == 'U') {
      this.router.navigate(['user']);
    }
    else if (this.auth.isLoggedIn() && localStorage.getItem('userType') == 'A') {
      this.router.navigate(['admin']);
    }
    else {
      this.initialiseForm();
    }
  }
  initialiseForm() {
    this.loginForm = this.fb.group({
      "email": ['', [Validators.required, Validators.email]],
      "password": ['', Validators.required]
    })
  }
  login() {
    let email = this.loginForm.controls['email'].value;
    this.authenticationService.getUserDetailsByEmail(email).subscribe((res: any) => {
      if (res.data) {
        if (res.data.password == this.loginForm.controls['password'].value) {
          localStorage.setItem('userLoggedIn', 'true');
          localStorage.setItem('userName', res.data.name);
          localStorage.setItem('userType', res.data.role);
          if (res.data.role == 'U')
            this.router.navigate(['user']);
          else
            this.router.navigate(['admin']);

        }
        else {
          let data: any = {};
          data.reason = Constants.INVALID_CREDENTIALS;
          data.showMessage = true;
          this.errorService.openDialog(data);
        }
      }
    })
  }
  forgotPassword() {
    const dialog = this.dialog.open(ForgotPasswordComponent)
  }
  signUp() {
    this.router.navigate(['login-home/register']);
  }
  loginHome() {
    this.router.navigate(['login-home']);
  }

}
