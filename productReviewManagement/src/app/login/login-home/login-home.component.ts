import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AuthenticationService } from 'src/app/services/authentication-service/authentication.service';
import { Constants } from 'src/app/shared/constants/constants';
import { ErrorDialogService } from 'src/app/shared/error-dialog/error-dialog.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';

@Component({
  selector: 'app-login-home',
  templateUrl: './login-home.component.html',
  styleUrls: ['./login-home.component.scss']
})
export class LoginHomeComponent implements OnInit {
  loginForm!: FormGroup;
  stats:any
  constructor(private errorService: ErrorDialogService, private dialog: MatDialog, private auth: AuthService, private router: Router, private fb: FormBuilder, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.authenticationService.getStats().subscribe((res:any)=>{
      this.stats=res.data;
    })    
  }
  loginPage(){
    this.router.navigate(['login-home/login'])
  }
}
