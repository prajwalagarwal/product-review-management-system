import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserModel } from 'src/app/models/user.model';
import { Constants } from 'src/app/shared/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  baseURL: string;
  constructor(private http: HttpClient) {
    this.baseURL = Constants.BASE_URL;
  }

  getUserDetailsByEmail(email: String) {
    const url = this.baseURL + Constants.LOGIN_API_END_POINT + "login";
    return this.http.put(url,email);
  }

  registerUser(userModel:UserModel){
    const url = this.baseURL + Constants.LOGIN_API_END_POINT + "sign-up";
    return this.http.post(url,userModel);
  }
 
  forgotPassword(userModel:UserModel){
    const url = this.baseURL + Constants.LOGIN_API_END_POINT + "forgot-password/"+userModel.email;
    return this.http.put(url,userModel);
  }

  getStats(){
    const url = this.baseURL + Constants.PRODUCT_API_END_POINT + "get-stats";
    return this.http.get(url);
  }
}
