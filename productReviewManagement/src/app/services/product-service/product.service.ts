import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/product.mode';
import { Constants } from 'src/app/shared/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  baseURL: string;
  constructor(private http: HttpClient) {
    this.baseURL = Constants.BASE_URL;
  }

  getProductsDetailsByProductCode(productCode: number) {
    const url = this.baseURL + Constants.PRODUCT_API_END_POINT + productCode;
    return this.http.get(url);
  }

  getAllProductsDetails() {
    const url = this.baseURL + Constants.PRODUCT_API_END_POINT;
    return this.http.get(url);
  }

  addProductForReview(product: Product) {
    const url = this.baseURL + Constants.PRODUCT_API_END_POINT + "add-product";
    return this.http.post(url, product);
  }
  getSearchResults(keyWord: string) {
    const url = this.baseURL + Constants.PRODUCT_API_END_POINT + "search-products";
    return this.http.get(url, {
      params: {
        "keyWord": keyWord
      }
    });
  }

}
