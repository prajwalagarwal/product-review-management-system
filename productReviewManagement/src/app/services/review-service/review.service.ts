import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Review } from 'src/app/models/Review.model';
import { Constants } from 'src/app/shared/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  baseURL: string;
  constructor(private http: HttpClient) {
    this.baseURL = Constants.BASE_URL;
  }

  postReview(productCode:number,review:Review){
    const url = this.baseURL + Constants.REVIEW_API_END_POINT + "add-review/"+productCode;
    return this.http.post(url,review);
  }

  deleteReview(reviewId:number){
    const url = this.baseURL + Constants.REVIEW_API_END_POINT +reviewId;
    return this.http.delete(url);
  }

  approveReview(reviewId:number,review:Review){
    const url = this.baseURL + Constants.REVIEW_API_END_POINT +reviewId;
    return this.http.put(url,review);
  }

  getPendingReviews(){
    const url = this.baseURL + Constants.REVIEW_API_END_POINT +'pending-reviews/';
    return this.http.get(url);
  }
}
