import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  setSession(){
    this.logOut();
    localStorage.setItem('userLoggedIn','true');
  }

  logOut(){
    localStorage.clear();
    sessionStorage.clear();
  }

  public isLoggedIn(){
    let isLoggedIn:any;
    isLoggedIn= JSON.parse(localStorage.getItem('userLoggedIn') || 'false');
    if(isLoggedIn)
      return true
    else
      return false;
  }

}
