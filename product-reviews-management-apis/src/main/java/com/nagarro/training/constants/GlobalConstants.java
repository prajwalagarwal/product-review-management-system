package com.nagarro.training.constants;

public class GlobalConstants {
	public static String NO_USER = "No user present with this email";
	public static String USER_EXIST = "User already present with this email";
	public static String PRODUCT_EXIST = "Product already present with this code";
	public static String NO_PRODUCT = "No product present with this code";
	public static String NO_REVIEW = "No such review present";
}
