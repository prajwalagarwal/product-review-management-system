package com.nagarro.training.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.converter.Converter;
import com.nagarro.training.entity.ProductEntity;
import com.nagarro.training.model.ProductModel;
import com.nagarro.training.model.Stats;
import com.nagarro.training.repository.AuthenticationDao;
import com.nagarro.training.repository.ProductDao;
import com.nagarro.training.repository.ReviewDao;
import com.nagarro.training.service.ProductsService;

@Service
public class ProductsServiceImplementation implements ProductsService {
	@Autowired
	ProductDao productDao;
	@Autowired
	AuthenticationDao authenticationDao;
	@Autowired
	ReviewDao reviewDao;
	@Autowired
	Converter converter;

	@Override
	public ResponseEntity<Object> addProductForReview(ProductModel productModel) {
		Optional<ProductEntity> productEntityPresent = productDao.findById(productModel.getProductCode());
		if (productEntityPresent.isPresent()) {
			return ResponseHandler.generateResponse(GlobalConstants.PRODUCT_EXIST, HttpStatus.BAD_REQUEST,
					null);
		}
		try {
			ProductEntity productEntity = converter.productModelToProductEntity(productModel);
			ProductEntity returnedproductEntity = productDao.save(productEntity);
			return ResponseHandler.generateResponse("success", HttpStatus.OK,
					converter.productEntityToProductModel(returnedproductEntity));
		} catch (Exception e) {
			return ResponseHandler.generateResponse("error", HttpStatus.INTERNAL_SERVER_ERROR, null);
		}
	}

	@Override
	public ResponseEntity<Object> getAllProductsDetails() {
		try {
			List<ProductEntity> productEntityList = new ArrayList<ProductEntity>();
			List<ProductModel> productModelList = new ArrayList<ProductModel>();
			productDao.findAll().forEach(productEntityList::add);

			for (ProductEntity productEntity : productEntityList) {
				productModelList.add(converter.productEntityToProductModel(productEntity));
			}
			if (productModelList.isEmpty()) {
				return ResponseHandler.generateResponse("success", HttpStatus.NO_CONTENT, null);
			}
			return ResponseHandler.generateResponse("success", HttpStatus.OK, productModelList);
		} catch (Exception e) {
			return ResponseHandler.generateResponse("error", HttpStatus.INTERNAL_SERVER_ERROR, null);
		}
	}

	@Override
	public ResponseEntity<Object> getProductsDetailsByProductCode(int productCode) {
		Optional<ProductEntity> productEntity = productDao.findById(productCode);
		if (productEntity.isPresent()) {
			ProductModel productModel = converter.productEntityToProductModel(productEntity.get());
			return ResponseHandler.generateResponse("success", HttpStatus.OK, productModel);
		} else {
			return ResponseHandler.generateResponse(GlobalConstants.NO_PRODUCT, HttpStatus.NOT_FOUND, null);
		}
	}

	@Override
	public ResponseEntity<Object> getStats() {
		Stats stats = new Stats();
		stats.setNoOfProducts(productDao.count());
		stats.setNoOfReviews(reviewDao.count());
		stats.setNoOfUsers(authenticationDao.count());
		return ResponseHandler.generateResponse("success", HttpStatus.OK, stats);
	}

	@Override
	public ResponseEntity<Object> getProductsResultForNameBrandOrCode(String keyWord) {
		Set<ProductEntity> searchedProductsEntity = productDao.getSearchResults(keyWord);
		Set<ProductModel> productModel=new HashSet<ProductModel>();
		searchedProductsEntity.forEach((ProductEntity productEntity)->{
			productModel.add(converter.productEntityToProductModel(productEntity));
		});
		return ResponseHandler.generateResponse("success", HttpStatus.OK, productModel);
	}

}
