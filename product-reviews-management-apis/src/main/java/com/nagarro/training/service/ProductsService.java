package com.nagarro.training.service;

import org.springframework.http.ResponseEntity;

import com.nagarro.training.model.ProductModel;

public interface ProductsService {
	public ResponseEntity<Object> getAllProductsDetails();

	public ResponseEntity<Object> getProductsDetailsByProductCode(int productCode);

	public ResponseEntity<Object> addProductForReview(ProductModel productModel);

	public ResponseEntity<Object> getStats();

	public ResponseEntity<Object> getProductsResultForNameBrandOrCode(String keyWord);

}
