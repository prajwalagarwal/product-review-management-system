package com.nagarro.training.service;

import org.springframework.http.ResponseEntity;

import com.nagarro.training.model.ReviewModel;

public interface ReviewService {
	public ResponseEntity<Object> postReview(int productCode, ReviewModel reviewModel);

	public ResponseEntity<Object> deleteReview(int reviewId);

	public ResponseEntity<Object> approveReview(int reviewId);

	public ResponseEntity<Object> getPendingReviews();

}
