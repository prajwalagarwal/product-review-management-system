package com.nagarro.training.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.converter.Converter;
import com.nagarro.training.entity.UserEntity;
import com.nagarro.training.model.UserModel;
import com.nagarro.training.repository.AuthenticationDao;
import com.nagarro.training.service.AuthenticationService;

@Service
public class AuthenticationServiceImplementation implements AuthenticationService {

	@Autowired
	AuthenticationDao authenticationDao;
	@Autowired
	Converter converter;

	@Override
	public ResponseEntity<Object> getUserByEmail(String id) {
		Optional<UserEntity> userEntity = authenticationDao.findById(id);
		if (userEntity.isPresent()) {
			UserModel userModel = converter.userEntityToUserModel(userEntity.get());
			return ResponseHandler.generateResponse("success", HttpStatus.OK, userModel);
		} else {
			return ResponseHandler.generateResponse(GlobalConstants.NO_USER, HttpStatus.NOT_FOUND, null);
		}
	}

	@Override
	public ResponseEntity<Object> registerNewUser(UserModel userModel) {
		Optional<UserEntity> userEntityPresent = authenticationDao.findById(userModel.getEmail());
		if (userEntityPresent.isPresent()) {
			return ResponseHandler.generateResponse(GlobalConstants.USER_EXIST, HttpStatus.BAD_REQUEST,
					null);
		}
		try {
			UserEntity userEntity = converter.userModelToUserEntity(userModel);
			UserEntity returnedUserEntity = authenticationDao.save(userEntity);
			return ResponseHandler.generateResponse("success", HttpStatus.OK,
					converter.userEntityToUserModel(returnedUserEntity));
		} catch (Exception e) {
			return ResponseHandler.generateResponse("error", HttpStatus.INTERNAL_SERVER_ERROR, null);
		}
	}

	@Override
	public ResponseEntity<Object> updateUser(String emailId, UserModel userModel) {
		Optional<UserEntity> userEntity = authenticationDao.findById(emailId);
		if (!userEntity.isPresent()) {
			return ResponseHandler.generateResponse(GlobalConstants.NO_USER, HttpStatus.BAD_REQUEST, null);
		}
		userEntity.get().setPassword(userModel.getPassword());
		UserEntity returnedUserEntity = authenticationDao.save(userEntity.get());
		return ResponseHandler.generateResponse("success", HttpStatus.OK,
				converter.userEntityToUserModel(returnedUserEntity));
	}
}
