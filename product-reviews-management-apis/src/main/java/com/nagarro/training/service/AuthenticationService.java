package com.nagarro.training.service;

import org.springframework.http.ResponseEntity;

import com.nagarro.training.model.UserModel;

public interface AuthenticationService {
	
	public ResponseEntity<Object> getUserByEmail(String emailId);
	public ResponseEntity<Object> registerNewUser(UserModel userModel);
	public ResponseEntity<Object> updateUser(String emaailId,UserModel userModel);
	

}
