package com.nagarro.training.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nagarro.training.constants.GlobalConstants;
import com.nagarro.training.converter.Converter;
import com.nagarro.training.entity.ProductEntity;
import com.nagarro.training.entity.ReviewEntity;
import com.nagarro.training.model.ReviewModel;
import com.nagarro.training.repository.ReviewDao;
import com.nagarro.training.service.ReviewService;

@Service
public class ReviewServiceImplementation implements ReviewService {

	@Autowired
	Converter converter;
	@Autowired
	ReviewDao reviewDao;

	@Override
	public ResponseEntity<Object> postReview(int productCode, ReviewModel reviewModel) {
		try {
			ReviewEntity reviewEntity = converter.reviewModelToReviewEntity(reviewModel);
			ProductEntity productEntity = new ProductEntity();
			productEntity.setProductCode(productCode);
			reviewEntity.setProduct(productEntity);
			ReviewEntity returnedReviewEntity = reviewDao.save(reviewEntity);
			return ResponseHandler.generateResponse("success", HttpStatus.OK,
					converter.reviewEntityToReviewModel(returnedReviewEntity));
		} catch (Exception e) {
			return ResponseHandler.generateResponse("error", HttpStatus.INTERNAL_SERVER_ERROR, null);
		}
	}

	@Override
	public ResponseEntity<Object> deleteReview(int reviewId) {
		try {
			reviewDao.deleteById(reviewId);
		} catch (Exception e) {
			return ResponseHandler.generateResponse(GlobalConstants.NO_REVIEW, HttpStatus.NOT_FOUND, null);
		}
		return ResponseHandler.generateResponse("success", HttpStatus.OK, null);
	}

	@Override
	public ResponseEntity<Object> approveReview(int reviewId) {
		ReviewEntity reviewEntity = reviewDao.findById(reviewId)
				.orElseThrow(() -> new ResourceNotFoundException(GlobalConstants.NO_REVIEW));
		reviewEntity.setReviewStatus(true);
		ReviewEntity returnedReviewEntity = reviewDao.save(reviewEntity);
		return ResponseHandler.generateResponse("success", HttpStatus.OK,
				converter.reviewEntityToReviewModel(returnedReviewEntity));
	}

	@Override
	public ResponseEntity<Object> getPendingReviews() {
		Set<ReviewEntity> reviewEntityList = reviewDao.getPendingReviews();
		Set<ReviewModel> reviewModelList = new HashSet<ReviewModel>();
		reviewDao.getPendingReviews().forEach(reviewEntityList::add);

		for (ReviewEntity reviewEntity : reviewEntityList) {
			reviewModelList.add(converter.reviewEntityToReviewModel(reviewEntity));
		}
		return ResponseHandler.generateResponse("success", HttpStatus.OK, reviewModelList);
	}
}
