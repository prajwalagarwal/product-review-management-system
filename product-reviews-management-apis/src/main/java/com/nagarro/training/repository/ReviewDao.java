package com.nagarro.training.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.webmvc.RepositoryRestController;

import com.nagarro.training.entity.ReviewEntity;

@RepositoryRestController
public interface ReviewDao extends JpaRepository<ReviewEntity, Integer> {

	@Query("SELECT p FROM ReviewEntity p where p.reviewStatus=FALSE")
	Set<ReviewEntity> getPendingReviews();

}
