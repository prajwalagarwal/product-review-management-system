package com.nagarro.training.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.nagarro.training.entity.ProductEntity;
import com.nagarro.training.entity.ReviewEntity;
import com.nagarro.training.entity.UserEntity;
import com.nagarro.training.model.ProductModel;
import com.nagarro.training.model.ReviewModel;
import com.nagarro.training.model.UserModel;

@Component
public class Converter {
	public UserModel userEntityToUserModel(UserEntity userEntity) {
		UserModel userModel = new UserModel(userEntity.getEmail(), userEntity.getPassword(), userEntity.getRole(),
				userEntity.getName());
		return userModel;
	}

	public UserEntity userModelToUserEntity(UserModel userModel) {
		UserEntity userEntity = new UserEntity(userModel.getEmail(), userModel.getPassword(), userModel.getRole(),
				userModel.getName());
		return userEntity;
	}

	public ProductModel productEntityToProductModel(ProductEntity productEntity) {
		ProductModel productModel = new ProductModel();
		productModel.setBrand(productEntity.getBrand());
		productModel.setProductName(productEntity.getProductName());
		productModel.setProductCode(productEntity.getProductCode());
		productModel.setReview(this.reviewEntityToReviewModel(productEntity.getReview()));
		return productModel;
	}

	public ProductEntity productModelToProductEntity(ProductModel productModel) {
		ProductEntity productEntity = new ProductEntity();
		productEntity.setBrand(productModel.getBrand());
		productEntity.setProductName(productModel.getProductName());
		productEntity.setProductCode(productModel.getProductCode());
		productEntity.setReview(this.reviewModelToReviewEntity(productModel.getReview()));
		return productEntity;
	}

	public Set<ReviewModel> reviewEntityToReviewModel(Set<ReviewEntity> setOfreviewEntity) {
		Set<ReviewModel> reviewModel = new HashSet<ReviewModel>();
		setOfreviewEntity.forEach((ReviewEntity reviewEntity) -> {
			reviewModel.add(new ReviewModel(reviewEntity.getReviewId(), reviewEntity.getRating(),
					reviewEntity.getHeading(), reviewEntity.getReviewComment(), reviewEntity.isReviewStatus()));
		});
		return reviewModel;
	}

	public Set<ReviewEntity> reviewModelToReviewEntity(Set<ReviewModel> setOfreviewModel) {
		Set<ReviewEntity> reviewEntity = new HashSet<ReviewEntity>();
		setOfreviewModel.forEach((ReviewModel reviewModel) -> {
			reviewEntity.add(new ReviewEntity(reviewModel.getReviewId(), reviewModel.getRating(),
					reviewModel.getHeading(), reviewModel.getReviewComment(), reviewModel.isReviewStatus()));
		});
		return reviewEntity;
	}

	public ReviewEntity reviewModelToReviewEntity(ReviewModel reviewModel) {
		ReviewEntity reviewEntity = new ReviewEntity(reviewModel.getReviewId(), reviewModel.getRating(),
				reviewModel.getHeading(), reviewModel.getReviewComment(), reviewModel.isReviewStatus());
		return reviewEntity;
	}

	public ReviewModel reviewEntityToReviewModel(ReviewEntity reviewEntity) {
		ReviewModel reviewModel = new ReviewModel(reviewEntity.getReviewId(), reviewEntity.getRating(),
				reviewEntity.getHeading(), reviewEntity.getReviewComment(), reviewEntity.isReviewStatus());
		return reviewModel;
	}
}
