package com.nagarro.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.training.model.UserModel;
import com.nagarro.training.service.AuthenticationService;

@RestController
@RequestMapping("product.review.management/v1/user")
@CrossOrigin(origins = "http://localhost:4200")
public class AuthenticationController {

	@Autowired
	AuthenticationService authenticationService;

	@PutMapping("/login")
	public ResponseEntity<Object> login(@RequestBody(required = true) String emailId) {
		return authenticationService.getUserByEmail(emailId);
	}

	@PostMapping("/sign-up")
	public ResponseEntity<Object> registerNewUser(@RequestBody(required = true) UserModel userModel) {
		return authenticationService.registerNewUser(userModel);
	}

	@PutMapping("/forgot-password/{emailId}")
	public ResponseEntity<Object> updateUser(@PathVariable("emailId") String emailId,
			@RequestBody(required = true) UserModel userModel) {
		return authenticationService.updateUser(emailId, userModel);
	}

}
