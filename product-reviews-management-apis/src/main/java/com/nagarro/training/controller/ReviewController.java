package com.nagarro.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.training.model.ReviewModel;
import com.nagarro.training.service.ReviewService;

@RestController
@RequestMapping("product.review.management/v1/review")
@CrossOrigin(origins = "http://localhost:4200")
public class ReviewController {

	@Autowired
	ReviewService reviewService;

	@PostMapping("/add-review/{productCode}")
	public ResponseEntity<Object> postReview(@PathVariable(value = "productCode", required = true) int productCode,
			@RequestBody(required = true) ReviewModel reviewModel) {
		return reviewService.postReview(productCode, reviewModel);
	}

	@DeleteMapping("/{reviewId}")
	public ResponseEntity<Object> deleteBook(@PathVariable(value = "reviewId", required = true) int reviewId) {
		return reviewService.deleteReview(reviewId);
	}

	@PutMapping("/{reviewId}")
	public ResponseEntity<Object> updateBook(@PathVariable(value = "reviewId") int reviewId,
			@RequestBody(required = true) ReviewModel reviewModel) {
		return reviewService.approveReview(reviewId);
	}

	@GetMapping("/pending-reviews")
	public ResponseEntity<Object> getPendingReviews() {
		return reviewService.getPendingReviews();
	}

}
