package com.nagarro.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.training.model.ProductModel;
import com.nagarro.training.service.ProductsService;

@RestController
@RequestMapping("product.review.management/v1/products")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {

	@Autowired
	ProductsService productService;

	@GetMapping("")
	public ResponseEntity<Object> getAllProductsDetails() {
		return productService.getAllProductsDetails();
	}

	@GetMapping("/{productCode}")
	public ResponseEntity<Object> getProductsDetailsByProductCode(
			@PathVariable(value = "productCode", required = true) int productCode) {
		return productService.getProductsDetailsByProductCode(productCode);
	}

	@PostMapping("/add-product")
	public ResponseEntity<Object> addProductForReview(@RequestBody(required = true) ProductModel productModel) {
		return productService.addProductForReview(productModel);
	}
	
	@GetMapping("/get-stats")
	public ResponseEntity<Object> getStats() {
		return productService.getStats();
	}
	@GetMapping("/search-products")
	public ResponseEntity<Object> getSearchResults(@RequestParam(required = true) String keyWord) {
		return productService.getProductsResultForNameBrandOrCode(keyWord);
	}
	
}
