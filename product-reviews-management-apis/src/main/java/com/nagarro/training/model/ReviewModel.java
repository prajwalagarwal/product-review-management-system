package com.nagarro.training.model;

public class ReviewModel {
	private int reviewId;
	private int rating;
	private String heading;
	private String reviewComment;
	private boolean reviewStatus;

	public ReviewModel() {
		super();
	}

	public ReviewModel(int reviewId, int rating, String heading, String reviewComment, boolean reviewStatus) {
		super();
		this.reviewId = reviewId;
		this.rating = rating;
		this.heading = heading;
		this.reviewComment = reviewComment;
		this.reviewStatus = reviewStatus;
	}

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getReviewComment() {
		return reviewComment;
	}

	public void setReviewComment(String reviewComment) {
		this.reviewComment = reviewComment;
	}

	public boolean isReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(boolean reviewStatus) {
		this.reviewStatus = reviewStatus;
	}
}
