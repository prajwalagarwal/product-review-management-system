package com.nagarro.training.model;

public class UserModel {
	String email;
	String password;
	char role;
	String name;

	public UserModel() {
		super();
	}

	public UserModel(String email, String password, char role, String name) {
		super();
		this.email = email;
		this.password = password;
		this.role = role;
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public char getRole() {
		return role;
	}

	public void setRole(char role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
