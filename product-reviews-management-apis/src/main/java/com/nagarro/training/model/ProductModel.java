package com.nagarro.training.model;

import java.util.Set;

public class ProductModel {

	private int productCode;
	private String productName;
	private String brand;
	private Set<ReviewModel> review;

	public ProductModel() {
		super();
	}

	public ProductModel(int productCode, String productName, String brand, Set<ReviewModel> review) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.brand = brand;
		this.review = review;
	}

	public int getProductCode() {
		return productCode;
	}

	public void setProductCode(int productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Set<ReviewModel> getReview() {
		return review;
	}

	public void setReview(Set<ReviewModel> review) {
		this.review = review;
	}
}
