package com.nagarro.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductReviewsManagementApisApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductReviewsManagementApisApplication.class, args);
	}

}
